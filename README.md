## `Typed MD Icons`
A list of all Google's material design icons' names.

This package provides several entities related to Google's material design icons (see [`material-design-icons`] package):

- `IconName`: union of all icons names (string literal types);
- `IconNameMap`: type of object, whose properties have both key and value equal to icon name
- `IconNameList`: type of array, that contains all icon names
- `map`: implementation of `IconNameMap`;
- `list`: implementation of `IconNameList`;

Use it in `.ts` or `.tsx` files like this:

```tsx
// React component definition
import React from "react";
import { IconName } from "typed-md-icons";

interface IconProps {
  children: IconName;
}

export default function Icon({ children, className, ...props }: IconProps) {
  return (
    <i {...props} className={"material-icons " + className}>
      {children}
    </i>
  );
}
```

```tsx
// React component usage
const accessibilityIcon = <Icon>accessibility_new<Icon/>;
```

  [`material-design-icons`]: https://www.npmjs.com/package/material-design-icons